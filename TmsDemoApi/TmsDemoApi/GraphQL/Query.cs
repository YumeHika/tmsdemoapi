﻿using System.Linq;
using TmsDemoApi.Model;

namespace TmsDemoApi.GraphQL
{
    public class Query
    {
        private readonly TmsContext dbContext;
        public Query(TmsContext dbContext)
        {
            this.dbContext = dbContext;
        }
        public IQueryable<User> Users => dbContext.User;
    }
}
