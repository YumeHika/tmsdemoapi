﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TmsDemoApi.Model
{
    public class TmsContext : DbContext
    {
        public TmsContext(DbContextOptions<TmsContext> options) : base(options)
        {

        }

        public DbSet<User> User { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<User>().HasKey(c => new { c.Id}).HasName("PK_User");
            modelBuilder.Entity<User>().Property(u => u.Id).HasColumnType("int").UseMySqlIdentityColumn().IsRequired();
            modelBuilder.Entity<User>().Property(u => u.Fullname).HasColumnType("varchar(500)").IsRequired();
            modelBuilder.Entity<User>().Property(u => u.Email).HasColumnType("varchar(500)").IsRequired();
            modelBuilder.Entity<User>().Property(u => u.Password).HasColumnType("varchar(50)").IsRequired();
            modelBuilder.Entity<User>().Property(u => u.Address).HasColumnType("varchar(1000)").IsRequired(false);
            modelBuilder.Entity<User>().Property(u => u.CreatedAt).HasColumnType("datetime").IsRequired();
            modelBuilder.Entity<User>().Property(u => u.UpdatedAt).HasColumnType("datetime").IsRequired();
        }
    }
}
