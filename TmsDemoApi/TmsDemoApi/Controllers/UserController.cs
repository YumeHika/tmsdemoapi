﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TmsDemoApi.Model;

namespace TmsDemoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private TmsContext _db;

        public UserController(TmsContext db)
        {
            _db = db;
        }

        [HttpGet("all")]
        public async Task<IActionResult> GetAll()
        {
            return Ok(this._db.User.ToList());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetOne(int id)
        {
            List<User> us = _db.User.Where(u => u.Id == id).ToList();
            return Ok(us);
        }

        [HttpPost("create/{id}")]
        public async Task<IActionResult> CreateOne(int id, User us)
        {
            var new_user = new User { Id = id, Address = us.Address, CreatedAt = us.CreatedAt, Email = us.Email, Fullname = us.Fullname, Password = us.Password, UpdatedAt = us.UpdatedAt };
            _db.User.Add(new_user);
            await _db.SaveChangesAsync();
            return Ok(_db.User.ToList());
        }

        [HttpPut("update/{user_no}")]
        public async Task<IActionResult> UpdateOne(int user_no, User us)
        {
            if (user_no != us.Id)
            {
                return BadRequest();
            }

            var todoItem = await _db.User.FindAsync(user_no);
            if (todoItem == null)
            {
                return NotFound();
            }

            todoItem.Password = us.Password;
            todoItem.Fullname = us.Fullname;
            todoItem.Email = us.Email;
            todoItem.Address = us.Address;
            todoItem.CreatedAt = us.CreatedAt;
            todoItem.UpdatedAt = us.UpdatedAt;

            await _db.SaveChangesAsync();


            return Ok();
        }

        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> DeleteOne(int id)
        {
            var user = await _db.User.FindAsync(id);
            _db.User.Remove(user);
            await _db.SaveChangesAsync();
            return Ok();

        }
    }
}
